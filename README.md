## Книга рецептов
Данное приложение разработано на Angular 8. Все рецепты хранятся в firebase. 
1. Перед началом работы, надо зарегистрироваться или авторизоваться https://prnt.sc/p6ny94. Email и пароль для теста test@test.ru 123456
2. Уже есть добавленные рецепты, получить их можно нажав сюда https://prnt.sc/p6h1zh.
3. Редактировать рецепт можно, нажав сюда https://prnt.sc/p6h2a6.
4. Добавить рецепт, можно нажав сюда https://prnt.sc/p6h2mg.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
